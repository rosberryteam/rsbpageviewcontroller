//Copyright (c) 2015 RSBPageViewController
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import "ViewController.h"

#import "RSBPageViewController.h"

#import "RSBRedViewController.h"
#import "RSBGreenViewController.h"
#import "RSBBlueViewController.h"
#import "RSBBlackViewController.h"

#import "RSBPlaceholderView.h"

@interface ViewController () <RSBPageViewControllerDataSource, RSBPageViewControllerDelegate>

@property (nonatomic) RSBPageViewController *pageViewController;
@property (nonatomic) NSArray *identifiers;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageViewController = [[RSBPageViewController alloc] initWithScrollDirection:RSBPageViewControllerScrollDirectionHorizontal];
    [self.view addSubview:self.pageViewController.view];
    
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    NSMutableArray *ma =[NSMutableArray array];
    for (NSInteger i = 0; i < 10; ++i) {
        NSString *identifier = @"blue";
        NSInteger index = [self indexFromNumber:0 toNumber:3];
        switch (index) {
            case 0:
                identifier = @"red";
                break;
                
            case 1:
                identifier = @"green";
                break;
        }
        [ma addObject:identifier];
    }
    [ma addObject:@"black"];
    self.identifiers = [NSArray arrayWithArray:ma];
    
    NSLog(@"%@", self.identifiers);
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.pageViewController reloadData];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.pageViewController.view.frame = self.view.bounds;
}

- (NSInteger)indexFromNumber:(NSInteger)fromNumber toNumber:(NSInteger)toNumber {
    return (arc4random() % (toNumber - fromNumber)) + fromNumber;
}

- (void)dealloc {
    NSLog(@"%s", __func__);
}

#pragma mark - RSBPageViewControllerDataSource

- (UIViewController<RSBPage> *)pageViewController:(RSBPageViewController *)pageViewController pageAtIndex:(NSUInteger)index {
    NSString *identifier = self.identifiers[index];
    RSBBaseViewController *baseViewController = (id)[pageViewController dequeueReusablePageWithReuseIdentifier:identifier ifNil:^UIViewController<RSBPage> * _Nonnull{
        if ([identifier isEqualToString:@"red"]) {
            return [[RSBRedViewController alloc] init];
        }
        else if ([identifier isEqualToString:@"green"]) {
            return [[RSBGreenViewController alloc] init];
        }
        else if ([identifier isEqualToString:@"blue"]) {
            return [[RSBBlueViewController alloc] init];
        }
        else {
            return [[RSBBlackViewController alloc] init];
        }
    }];
    return baseViewController;
}

- (NSUInteger)pageViewControllerNumberOfPages:(RSBPageViewController *)pageViewController {
    return self.identifiers.count;
}

- (NSUInteger)pageViewControllerNumberOfNearestLoadedPageViews:(RSBPageViewController *)pageViewController {
    return 2;
}

#pragma mark - RSBPageViewControllerDelegate

- (void)pageViewController:(RSBPageViewController *)pageViewController configurePage:(UIViewController<RSBPage> *)page atIndex:(NSUInteger)index {
    RSBBaseViewController *baseViewController = (id)page;
    baseViewController.titleLabel.text = [NSString stringWithFormat:@"Page %li configured", (long)index+1];
    if ([baseViewController isKindOfClass:[RSBBlackViewController class]]) {
        baseViewController.titleLabel.text = @"LOADING...";
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            __weak typeof(self) weakSelf = self;
            [pageViewController scrollToPageAtIndex:index-1 animated:YES completion:^(RSBPageViewController * _Nonnull __weak pageViewController) {
                NSMutableArray *ma = [NSMutableArray arrayWithArray:weakSelf.identifiers];
                [ma removeLastObject];
                weakSelf.identifiers = [NSArray arrayWithArray:ma];
                [pageViewController reloadData];
            }];
        });
    }
}

- (void)pageViewController:(RSBPageViewController *)pageViewController configurePlaceholderView:(UIView *)placeholderView forPage:(UIViewController<RSBPage> *)page atIndex:(NSUInteger)index {
    if ([page isKindOfClass:[RSBBlackViewController class]]) {
        RSBBaseViewController *baseViewController = (id)page;
        baseViewController.titleLabel.text = @"LOADING...";
    }
    else {
        RSBPlaceholderView *placeholder = (id)placeholderView;
        placeholder.titleLabel.text = [NSString stringWithFormat:@"Placeholder for page %li", (long)index+1];
    }
}

@end
