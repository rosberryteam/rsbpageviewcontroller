//Copyright (c) 2015 RSBPageViewController
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <Foundation/Foundation.h>
@class RSBPageViewController;
@protocol RSBPage;

/*!
 * @discussion The RSBPageViewControllerDataSource protocol is adopted by an object 
 * that mediates the application’s data model for a @c RSBPageViewController. 
 * The data source provides the object with the information it needs to construct 
 * and modify a pages of @c RSBPageViewController.
 */
@protocol RSBPageViewControllerDataSource <NSObject>

/*!
 * @discussion Provide a number of pages, which will be showed in @c RSBPageViewController.
 * @param pageViewController Instance of @c RSBPageViewController.
 * @return Number of pages.
 */
- (NSUInteger)pageViewControllerNumberOfPages:(RSBPageViewController *)pageViewController;
/*!
 * @discussion Provide a page which should be placed at index.
 * @param pageViewController Instance of @c RSBPageViewController.
 * @return Instance of @c UIViewController adoptped to @c RSBPage protocol.
 */
- (UIViewController<RSBPage> *)pageViewController:(RSBPageViewController *)pageViewController pageAtIndex:(NSUInteger)index;


@optional
/*!
 * @discussion You can modify number of nearest loaded pages of current displayed page. 
 * Default value is 1.
 * @param pageViewController Instance of @c RSBPageViewController.
 * @return Number of nearest loaded pages.
 */
- (NSUInteger)pageViewControllerNumberOfNearestLoadedPageViews:(RSBPageViewController *)pageViewController;

@end
