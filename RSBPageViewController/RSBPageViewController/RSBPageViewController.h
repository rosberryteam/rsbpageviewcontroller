//Copyright (c) 2015 RSBPageViewController
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "RSBPageViewControllerDataSource.h"
#import "RSBPageViewControllerDelegate.h"
#import "RSBPage.h"
#import "RSBPageViewControllerState.h"

/// Direction of paging.
typedef NS_ENUM(NSInteger, RSBPageViewControllerScrollDirection) {
    /// Pages will be placed horizontally (swipe right or left)
    RSBPageViewControllerScrollDirectionHorizontal,
    /// Pages will be placed vertically (swipe up or down)
    RSBPageViewControllerScrollDirectionVertical
};

NS_ASSUME_NONNULL_BEGIN

/*!
 * @discussion This view controller is replace for native @c UIPageViewController.
 * The way of work is similar with @c UITableView or @c UICollectionView.
 */
@interface RSBPageViewController : UIViewController 

/*!
 * @discussion Use this method to initialize view controller.
 * @param direction Direction of scrolling. @see RSBPageViewControllerScrollDirection.
 * @return Instance of @c RSBPageViewController.
 */
- (instancetype)initWithScrollDirection:(RSBPageViewControllerScrollDirection)direction NS_DESIGNATED_INITIALIZER;

/// An object which adopt @c RSBPageViewControllerDataSource protocol and used as data source.
@property (nonatomic, nullable, weak) id<RSBPageViewControllerDataSource> dataSource;
/// An object which adopt @c RSBPageViewControllerDelegate protocol and used as delegate.
@property (nonatomic, nullable, weak) id<RSBPageViewControllerDelegate> delegate;

/// Main scroll view of @c RSBPageViewController.
@property (nonatomic, readonly) UIScrollView *scrollView;
///If you need to be notified about scroll events - you are welcome.
@property (nonatomic, weak, nullable) id<UIScrollViewDelegate> scrollDelegate;

/*!
 * @discussion Current state of @c RSBPageViewController.
 * @warning This property is calculated. Do NOT use it in methods of @c RSBPageViewControllerDataSource.
 * It may cause lack of performance.
 */
@property (nonatomic, readonly) RSBPageViewControllerState *currentState;

/// Direction of pages scroll.
@property (nonatomic, readonly) RSBPageViewControllerScrollDirection scrollDirection;

/// Array of count active view controllers.
@property (nonatomic, readonly) NSArray<UIViewController<RSBPage> *> *currentActiveViewControllers;


/*!
 * @discussion This method works similar with @c UITableView or @c UICollectionView deque methods.
 */
- (UIViewController<RSBPage> *)dequeueReusablePageWithReuseIdentifier:(NSString *)identifier ifNil:(UIViewController<RSBPage> *(^)())createBlock;

/// Use this method for reload data.
- (void)reloadData;

/// Use this method to scroll to page at index.
- (void)scrollToPageAtIndex:(NSInteger)index animated:(BOOL)animated completion:(nullable void (^)(RSBPageViewController * _Nonnull __weak pageViewController))completion;

@end

NS_ASSUME_NONNULL_END
