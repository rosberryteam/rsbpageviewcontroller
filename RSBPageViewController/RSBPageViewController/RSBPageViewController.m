//Copyright (c) 2015 RSBPageViewController
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import "RSBPageViewController.h"
#import "RSBPageViewControllerDelegate.h"
#import "RSBPageViewControllerDataSource.h"
#import "RSBPage.h"

@interface RSBPageViewController () <UIScrollViewDelegate>

@property (nonatomic, readwrite) UIScrollView *scrollView;
@property (nonatomic, readwrite) RSBPageViewControllerScrollDirection scrollDirection;
@property (nonatomic, readwrite) RSBPageViewControllerState *currentState;
@property (nonatomic, readonly) NSUInteger currentIndex;

@property (nonatomic) NSUInteger numberOfPages;
@property (nonatomic) NSUInteger numberOfNearestLoadedPageViews;

@property (nonatomic) NSMutableDictionary *activeViewControllers;
@property (nonatomic) NSMutableDictionary *activePlaceholderViewControllers;
@property (nonatomic) NSMutableDictionary *reusableViewControllers;
@property (nonatomic) NSMutableDictionary *placeholderViews;
@property (nonatomic) NSMutableDictionary *cachedPages;
@property (nonatomic) NSMutableDictionary *appearedPages;

@property (nonatomic) UIView *contentView;

@property (nonatomic, copy) void(^scrollAnimationCompletionBlock)(__weak RSBPageViewController *pageViewController);

@end

@implementation RSBPageViewController

- (instancetype)initWithCoder:(NSCoder *)coder {
    return [self initWithScrollDirection:RSBPageViewControllerScrollDirectionHorizontal];
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    return [self initWithScrollDirection:RSBPageViewControllerScrollDirectionHorizontal];
}

- (instancetype)initWithScrollDirection:(RSBPageViewControllerScrollDirection)direction {
    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        self.reusableViewControllers = [NSMutableDictionary dictionary];
        self.activeViewControllers = [NSMutableDictionary dictionary];
        self.activePlaceholderViewControllers = [NSMutableDictionary dictionary];
        self.placeholderViews = [NSMutableDictionary dictionary];
        self.cachedPages = [NSMutableDictionary dictionary];
        self.appearedPages = [NSMutableDictionary dictionary];
        
        self.scrollDirection = direction;
    }
    
    return self;
}

- (void)dealloc {
    [self.reusableViewControllers removeAllObjects];
    [self.activeViewControllers removeAllObjects];
    [self.activePlaceholderViewControllers removeAllObjects];
    [self.placeholderViews removeAllObjects];
    [self.cachedPages removeAllObjects];
    [self.appearedPages removeAllObjects];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView = [[UIScrollView alloc] init];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    self.scrollView.delaysContentTouches = NO;
    self.scrollView.directionalLockEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.scrollView];
    
    self.contentView = [[UIView alloc] init];
    self.contentView.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.contentView];
    
    [self updatePagesNumbers];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.currentState.viewController beginAppearanceTransition:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.currentState.viewController endAppearanceTransition];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.currentState.viewController beginAppearanceTransition:NO animated:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.currentState.viewController endAppearanceTransition];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.scrollView.frame = self.view.bounds;
    [self updateScrollContentSizeIfNeeded];
}

- (void)loadCurrentViewAndNearestViews {
    //Get all nearest pages indexes.
    NSInteger currentIndex = self.currentIndex;
    NSInteger startIndex = currentIndex - self.numberOfNearestLoadedPageViews;
    if (startIndex < 0) {
        startIndex = 0;
    }
    NSUInteger endIndex = currentIndex + self.numberOfNearestLoadedPageViews;
    
    if (endIndex >= self.numberOfPages) {
        endIndex = self.numberOfPages - 1;
    }
    
    //Send disappear events for needed pages.
    for (NSNumber *key in self.activeViewControllers.allKeys) {
        NSUInteger index = [key unsignedIntegerValue];
        if (index == currentIndex) {
            continue;
        }
        if (self.appearedPages[key]) {
            [self sendAnimatedDisappearForPageAtIndex:index];
        }
    }
    
    //Check if we can to use already active pages.
    NSMutableDictionary *keepPages = [NSMutableDictionary dictionary];
    NSMutableArray *keepPagesViews = [NSMutableArray array];
    NSInteger checkFromIndex = currentIndex - self.numberOfNearestLoadedPageViews;
    if (checkFromIndex < 0) {
        checkFromIndex = 0;
    }
    NSInteger checkToIndex = currentIndex + self.numberOfNearestLoadedPageViews;
    if (checkToIndex >= self.numberOfPages) {
        checkToIndex = self.numberOfPages - 1;
    }
    
    for (NSInteger i = checkFromIndex; i < checkToIndex + 1; ++i) {
        UIViewController<RSBPage> *page = self.activeViewControllers[@(i)];
        if (page) {
            [keepPages setObject:page forKey:@(i)];
            [keepPagesViews addObject:page.view];
        }
    }
    
    //Don't remove views of already active pages.
    BOOL havePagesToKeep = keepPages.allValues.count > 0;
    for (UIView *sv in self.contentView.subviews) {
        if (havePagesToKeep) {
            if ([keepPagesViews containsObject:sv]) {
                continue;
            }
        }
        [sv removeFromSuperview];
    }
    
    //If page at current index isn't active we see placeholder of this page.
    //So we try to use placeholder's page instead of reuse some random page.
    [self.cachedPages removeAllObjects];
    if (![self pageAtIndexIsActive:currentIndex]) {
        UIViewController<RSBPage> *page = self.activePlaceholderViewControllers[@(currentIndex)];
        if (page) {
            [self.cachedPages setObject:page forKey:@(currentIndex)];
        }
    }
    
    //Remove pages which won't be active from parent view controller
    for (NSNumber *key in self.activeViewControllers.allKeys) {
        if (!keepPages[key]) {
            UIViewController<RSBPage> *page = self.activeViewControllers[key];
            [page removeFromParentViewController];
        }
    }
    
    //Restore pages which are already active.
    [self.activeViewControllers removeAllObjects];
    if (havePagesToKeep) {
        self.cachedPages = [NSMutableDictionary dictionaryWithDictionary:keepPages];
        self.activeViewControllers = [NSMutableDictionary dictionaryWithDictionary:keepPages];
    }
    
    //If page at current index isn't active - make it active.
    if (![self pageAtIndexIsActive:currentIndex]) {
        [self addViewOfPageAtIndexAsSubview:currentIndex];
        if (!self.appearedPages[@(currentIndex)]) {
            [self sendAnimatedAppearForPageAtIndex:currentIndex];
        }
    }
    
    //Load all other pages to active state if needed.
    for (NSUInteger index = startIndex; index < endIndex + 1; ++index) {
        if (index == currentIndex || [self pageAtIndexIsActive:index]) {
            continue;
        }
        
        [self addViewOfPageAtIndexAsSubview:index];
    }
    
    [self.activePlaceholderViewControllers removeAllObjects];
}

#pragma mark - Forwarding Appearance Methods

- (BOOL)shouldAutomaticallyForwardAppearanceMethods {
    return NO;
}

#pragma mark - Reload

- (void)reloadData {
    [self updatePagesNumbers];
    
    for (NSNumber *key in self.appearedPages.allKeys) {
        NSUInteger index = [key unsignedIntegerValue];
        if (self.appearedPages[key]) {
            [self sendAnimatedDisappearForPageAtIndex:index];
        }
    }
    [self.appearedPages removeAllObjects];
    
    for (UIView *sv in self.contentView.subviews) {
        [sv removeFromSuperview];
    }
    
    CGPoint offset = self.scrollView.contentOffset;
    CGSize contentSize = [self scrollContentSize];
    self.scrollView.contentSize = contentSize;
    self.contentView.frame = CGRectMake(0, 0, contentSize.width, contentSize.height);
    [self.scrollView setContentOffset:offset animated:NO];
    
    [self.placeholderViews removeAllObjects];
    [self.activePlaceholderViewControllers removeAllObjects];
    [self.cachedPages removeAllObjects];
    [self.activeViewControllers removeAllObjects];
    
    [self loadCurrentViewAndNearestViews];
}

#pragma mark - Scroll

- (void)scrollToPageAtIndex:(NSInteger)index animated:(BOOL)animated completion:(nullable void (^)(RSBPageViewController * _Nonnull __weak))completion {
    if (index < self.numberOfPages && index >= 0) {
        NSUInteger currentIndex = self.currentIndex;
        if (index != currentIndex) {
            [self updateScrollContentSizeIfNeeded];
            
            double widthValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? CGRectGetWidth(self.scrollView.bounds) : CGRectGetHeight(self.scrollView.bounds);
            double offsetValue = widthValue * index;
            double x = 0;
            double y = 0;
            if (self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal) {
                x = offsetValue;
            }
            else {
                y = offsetValue;
            }
            if (animated) {
                self.scrollAnimationCompletionBlock = completion;
            }
            [self.scrollView setContentOffset:CGPointMake(x, y) animated:animated];
            if (!animated) {
                [self didFinishScrolling];
                if (completion) {
                    __weak typeof(self) weakSelf = self;
                    completion(weakSelf);
                }
            }
        }
    }
}

#pragma mark - Setters/Getters

- (NSUInteger)currentIndex {
    CGPoint offset = self.scrollView.contentOffset;
    double offsetValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? offset.x : offset.y;
    double widthValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? CGRectGetWidth(self.scrollView.bounds) : CGRectGetHeight(self.scrollView.bounds);
    return offsetValue / widthValue;
}

- (UIViewController<RSBPage> *)currentViewController {
    return [self pageAtIndex:self.currentIndex];
}

- (RSBPageViewControllerState *)currentState {
    if (!_currentState) {
        _currentState = [[RSBPageViewControllerState alloc] init];
    }
    
    CGPoint offset = self.scrollView.contentOffset;
    double offsetValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? offset.x : offset.y;
    double widthValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? CGRectGetWidth(self.scrollView.bounds) : CGRectGetHeight(self.scrollView.bounds);
    _currentState.index = offsetValue / widthValue + 0.5;
    _currentState.viewController = [self pageAtIndex:_currentState.index];
    
    return _currentState;
}

- (NSArray<UIViewController<RSBPage> *> *)currentActiveViewControllers {
    return self.activeViewControllers.allValues;
}

#pragma mark - Appearance

- (void)sendAppearance:(BOOL)appear animated:(BOOL)animated forPage:(UIViewController<RSBPage> *)page {
    [page beginAppearanceTransition:appear animated:animated];
    [page endAppearanceTransition];
}

- (void)sendAnimatedAppearForPageAtIndex:(NSUInteger)index {
    UIViewController<RSBPage> *page = self.appearedPages[@(index)];
    if (!page) {
        page = [self pageAtIndex:index];
    }
    [self sendAppearance:YES animated:YES forPage:page];
    [self.appearedPages setObject:page forKey:@(index)];
}

- (void)sendAnimatedDisappearForPageAtIndex:(NSUInteger)index {
    UIViewController<RSBPage> *page = self.appearedPages[@(index)];
    if (!page) {
        page = [self pageAtIndex:index];
    }
    [self sendAppearance:NO animated:YES forPage:page];
    [self.appearedPages removeObjectForKey:@(index)];
}

#pragma mark - Helpers

- (void)updateScrollContentSizeIfNeeded {
    if (CGSizeEqualToSize(self.scrollView.contentSize, CGSizeZero)) {
        CGSize contentSize = [self scrollContentSize];
        self.scrollView.contentSize = contentSize;
        self.contentView.frame = CGRectMake(0, 0, contentSize.width, contentSize.height);
    }
}

- (void)updatePagesNumbers {
    self.numberOfPages = [self.dataSource pageViewControllerNumberOfPages:self];
    if ([self.dataSource respondsToSelector:@selector(pageViewControllerNumberOfNearestLoadedPageViews:)]) {
        self.numberOfNearestLoadedPageViews = [self.dataSource pageViewControllerNumberOfNearestLoadedPageViews:self];
    }
    
    if (self.numberOfNearestLoadedPageViews <= 0) {
        self.numberOfNearestLoadedPageViews = 1;
    }
}

- (void)addViewOfPageAtIndexAsSubview:(NSUInteger)index {
    UIViewController<RSBPage> *page = [self pageAtIndex:index];
    [self.activeViewControllers setObject:page forKey:@(index)];
    
    [self addChildViewController:page];
    UIView *view = page.view;
    [self.contentView addSubview:view];
    view.frame = [self frameForPageAtIndex:index];
    
    [self.delegate pageViewController:self configurePage:page atIndex:index];
    
    UIView *placeholderView = page.placeholderView;
    if (![placeholderView isEqual:view]) {
        [placeholderView removeFromSuperview];
    }
    [self.placeholderViews removeObjectForKey:@(index)];
}

- (void)addPlaceholderOfPageAtIndexAsAsubview:(NSUInteger)index {
    UIViewController<RSBPage> *page = [self pageAtIndex:index];
    [self.activePlaceholderViewControllers setObject:page forKey:@(index)];
    
    UIView *placeholderView = page.placeholderView;
    if (![self.placeholderViews[@(index)] isEqual:placeholderView]) {
        placeholderView.frame = [self frameForPageAtIndex:index];
        [self.contentView addSubview:placeholderView];
        [self.placeholderViews setObject:placeholderView forKey:@(index)];
        
        if ([self.delegate respondsToSelector:@selector(pageViewController:configurePlaceholderView:forPage:atIndex:)]) {
            [self.delegate pageViewController:self configurePlaceholderView:placeholderView forPage:page atIndex:index];
        }
    }
}

- (UIViewController<RSBPage> *)pageAtIndex:(NSUInteger)index {
    UIViewController<RSBPage> *page = self.cachedPages[@(index)];
    if (!page) {
        page = [self.dataSource pageViewController:self pageAtIndex:index];
        [self.cachedPages setObject:page forKey:@(index)];
    }
    return page;
}

- (BOOL)pageAtIndexIsActive:(NSUInteger)index {
    return self.activeViewControllers[@(index)] != nil;
}

- (void)layoutPagesForScrollView:(UIScrollView *)scrollView {
#ifdef DEBUG
    BOOL needLogPageNumbers = NO;
#endif
    
    CGPoint offset = scrollView.contentOffset;
    double offsetValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? offset.x : offset.y;
    
    NSInteger currentIndex = self.currentIndex;
    NSInteger nextIndex = currentIndex + 1;
    NSInteger previousIndex = currentIndex - 1;
    
    if ([self isScrollViewScrollingForward:scrollView]) { //Moving to the right
        if (nextIndex >= self.numberOfPages) {
            return;
        }
        
        CGRect nextPageFrame = [self frameForPageAtIndex:nextIndex];
        
        double originValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? nextPageFrame.origin.x : nextPageFrame.origin.y;
        double pageSizeValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? CGRectGetWidth(scrollView.bounds) : CGRectGetHeight(scrollView.bounds);
        double maxOffset = (offsetValue + pageSizeValue);
        
        if (maxOffset - originValue >= 0) { //When scroll is about to show next page
            if (previousIndex >= 0 && previousIndex != currentIndex) {
                if ([self pageAtIndexIsActive:previousIndex]) {
                    if (self.appearedPages[@(previousIndex)]) {
                        [self sendAnimatedDisappearForPageAtIndex:previousIndex];
                    }
                }
            }
            
            --previousIndex;
            if (previousIndex >= 0 && previousIndex != currentIndex) {
                if (![self pageAtIndexIsActive:previousIndex]) {
                    [self.activePlaceholderViewControllers removeObjectForKey:@(previousIndex)];
                    [self.placeholderViews removeObjectForKey:@(previousIndex)];
                    [self.cachedPages removeObjectForKey:@(previousIndex)];
                }
            }
            
#ifdef DEBUG
            if (needLogPageNumbers) {
                ++previousIndex;
            }
#endif
            
            if (![self pageAtIndexIsActive:nextIndex]) {
                [self addPlaceholderOfPageAtIndexAsAsubview:nextIndex];
            }
            else {
                if (!self.appearedPages[@(nextIndex)]) {
                    [self sendAnimatedAppearForPageAtIndex:nextIndex];
                }
            }
            
            //We should load one more placeholder because of
            //currentIndex dynamic calculating
            ++nextIndex;
            if (nextIndex < self.numberOfPages) {
                if (![self pageAtIndexIsActive:nextIndex]) {
                    [self addPlaceholderOfPageAtIndexAsAsubview:nextIndex];
                }
            }
            
#ifdef DEBUG
            if (needLogPageNumbers) {
                --nextIndex;
            }
#endif
        }
    }
    else {//Moving to the left
        
        //Because of currentIndex dynamically calculating we should
        //increment all indexes when user scrolling backward
        ++previousIndex;
        ++currentIndex;
        ++nextIndex;
        
        CGRect currentPageFrame = [self frameForPageAtIndex:currentIndex];
        
        double originValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? currentPageFrame.origin.x : currentPageFrame.origin.y;
        
        if (previousIndex >= 0 && previousIndex != currentIndex) {
            if (originValue - offsetValue >= 0) { //When scroll is about to show previous page
                if (nextIndex < self.numberOfPages && nextIndex != currentIndex) {
                    if ([self pageAtIndexIsActive:nextIndex]) {
                        if (self.appearedPages[@(nextIndex)]) {
                            [self sendAnimatedDisappearForPageAtIndex:nextIndex];
                        }
                    }
                }
                
                ++nextIndex;
                if (nextIndex < self.numberOfPages && nextIndex != currentIndex) {
                    if (![self pageAtIndexIsActive:nextIndex]) {
                        [self.activePlaceholderViewControllers removeObjectForKey:@(nextIndex)];
                        [self.placeholderViews removeObjectForKey:@(nextIndex)];
                        [self.cachedPages removeObjectForKey:@(nextIndex)];
                    }
                }
                
#ifdef DEBUG
                if (needLogPageNumbers) {
                    --nextIndex;
                }
#endif
                
                if (![self pageAtIndexIsActive:previousIndex]) {
                    [self addPlaceholderOfPageAtIndexAsAsubview:previousIndex];
                }
                else {
                    if (!self.appearedPages[@(previousIndex)]) {
                        [self sendAnimatedAppearForPageAtIndex:previousIndex];
                    }
                }
            }
        }
    }
    
#ifdef DEBUG
    if (needLogPageNumbers) {
        NSLog(@"[%li] - [%li] - [%li]", (long)previousIndex, (long)currentIndex, (long)nextIndex);
    }
#endif
}

#pragma mark - Layout methods

- (CGSize)scrollContentSize {
    double width = CGRectGetWidth(self.scrollView.bounds);
    double height = CGRectGetHeight(self.scrollView.bounds);
    
    switch (self.scrollDirection) {
        case RSBPageViewControllerScrollDirectionHorizontal:
            width *= self.numberOfPages;
            break;
            
        case RSBPageViewControllerScrollDirectionVertical:
            height *= self.numberOfPages;
            break;
    }
    
    return CGSizeMake(width, height);
}

- (CGRect)frameForPageAtIndex:(NSUInteger)index {
    double width = CGRectGetWidth(self.scrollView.bounds);
    double height = CGRectGetHeight(self.scrollView.bounds);
    double x = 0;
    double y = 0;
    
    switch (self.scrollDirection) {
        case RSBPageViewControllerScrollDirectionHorizontal:
            x = width * index;
            break;
            
        case RSBPageViewControllerScrollDirectionVertical:
            y = height * index;
            break;
    }
    
    return CGRectMake(x, y, width, height);
}

- (BOOL)isScrollViewScrollingForward:(UIScrollView *)scrollView {
    CGPoint velocity = [scrollView.panGestureRecognizer velocityInView:scrollView];
    double velocityValue = self.scrollDirection == RSBPageViewControllerScrollDirectionHorizontal ? velocity.x : velocity.y;
    return velocityValue < 0;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView.isTracking) {
        return;
    }
    
    [self didFinishScrolling];
    
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndDecelerating:)]){
        [self.scrollDelegate scrollViewDidEndDecelerating:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self didFinishScrolling];
    }
    else {
        [self layoutPagesForScrollView:scrollView];
    }
    
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)]){
        [self.scrollDelegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self layoutPagesForScrollView:scrollView];
    
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidScroll:)]){
        [self.scrollDelegate scrollViewDidScroll:scrollView];
    }
    
    RSBPageViewControllerState *state = self.currentState;
}

- (void)didFinishScrolling {
    [self loadCurrentViewAndNearestViews];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    if (self.scrollAnimationCompletionBlock) {
        __weak typeof(self) weakSelf = self;
        self.scrollAnimationCompletionBlock(weakSelf);
    }
    [self didFinishScrolling];
    
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndScrollingAnimation:)]){
        [self.scrollDelegate scrollViewDidEndScrollingAnimation:scrollView];
    }
}

#pragma mark - Reusable

- (UIViewController<RSBPage> *)dequeueReusablePageWithReuseIdentifier:(NSString *)identifier ifNil:(UIViewController<RSBPage> *(^)())createBlock {
    NSArray *viewControllersWithReuseIdentifier = self.reusableViewControllers[identifier];
    if (!viewControllersWithReuseIdentifier) {
        viewControllersWithReuseIdentifier = @[];
    }
    NSArray *activePages = self.activeViewControllers.allValues;
    NSArray *activePlaceholders = self.activePlaceholderViewControllers.allValues;
    
    UIViewController<RSBPage> *dequeuedPage = nil;
    for (UIViewController<RSBPage> *page in viewControllersWithReuseIdentifier) {
        if (![activePages containsObject:page] && ![activePlaceholders containsObject:page]) {
            dequeuedPage = page;
            break;
        }
    }
    
    if (!dequeuedPage) {
        dequeuedPage = createBlock();
        viewControllersWithReuseIdentifier = [viewControllersWithReuseIdentifier arrayByAddingObject:dequeuedPage];
        [self.reusableViewControllers setObject:viewControllersWithReuseIdentifier forKey:identifier];
    }
    
    [dequeuedPage prepareForReuse];
    return dequeuedPage;
}

#pragma mark - Other UIScrollViewDelegate methods

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidZoom:)]){
        [self.scrollDelegate scrollViewDidZoom:scrollView];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillBeginDragging:)]){
        [self.scrollDelegate scrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillEndDragging:withVelocity:targetContentOffset:)]){
        [self.scrollDelegate scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillBeginDecelerating:)]){
        [self.scrollDelegate scrollViewWillBeginDecelerating:scrollView];
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(viewForZoomingInScrollView:)]){
        return [self.scrollDelegate viewForZoomingInScrollView:scrollView];
    }
    return nil;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewWillBeginZooming:withView:)]){
        [self.scrollDelegate scrollViewWillBeginZooming:scrollView withView:view];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidEndZooming:withView:atScale:)]){
        [self.scrollDelegate scrollViewDidEndZooming:scrollView withView:view atScale:scale];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewShouldScrollToTop:)]){
        return [self.scrollDelegate scrollViewShouldScrollToTop:scrollView];
    }
    return YES;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    if ([self.scrollDelegate respondsToSelector:@selector(scrollViewDidScrollToTop:)]){
        [self.scrollDelegate scrollViewDidScrollToTop:scrollView];
    }
}

@end
