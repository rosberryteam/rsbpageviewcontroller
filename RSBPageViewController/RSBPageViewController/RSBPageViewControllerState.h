//
//  RSBPageViewControllerState.h
//  RSBPageViewController
//
//  Created by Anton Kovalev on 26.07.16.
//  Copyright © 2016 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
@class UIViewController;
@protocol RSBPage;

/*!
 * @discussion This class created for getting state of @c RSBPageViewController.
 */
@interface RSBPageViewControllerState : NSObject

/// View controller associated with state.
@property (nonatomic) UIViewController<RSBPage> *viewController;
/// Index associated with state.
@property (nonatomic) NSInteger index;

@end
