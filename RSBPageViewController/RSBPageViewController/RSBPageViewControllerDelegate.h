//Copyright (c) 2015 RSBPageViewController
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "RSBPage.h"

/*!
 * @discussion The delegate of a @c RSBPageViewController must adopt 
 * the RSBPageViewControllerDelegate protocol.
 */
@protocol RSBPageViewControllerDelegate <NSObject>

/*!
 * @discussion This method will be called when @c page at @c index
 * must be configured.
 * @param pageViewController The instance of @c RSBPageViewController.
 * @param page The page which should be configured.
 * @param index Index of the page.
 */
- (void)pageViewController:(RSBPageViewController *)pageViewController configurePage:(UIViewController<RSBPage> *)page atIndex:(NSUInteger)index;

@optional
/*!
 * @discussion This method will be called when placeholder of page 
 * should be configured.
 * @param pageViewController The instance of @c RSBPageViewController.
 * @param placeholderView Instance of placeholder's view.
 * @param page The page which placeholder should be configured.
 * @param index Index of the page.
 */
- (void)pageViewController:(RSBPageViewController *)pageViewController configurePlaceholderView:(UIView *)placeholderView forPage:(UIViewController<RSBPage> *)page atIndex:(NSUInteger)index;

@end
