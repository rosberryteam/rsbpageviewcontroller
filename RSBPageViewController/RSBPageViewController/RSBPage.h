//Copyright (c) 2015 RSBPageViewController
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.

#import <Foundation/Foundation.h>

/*!
 * @discussion Every instance of UIViewController which will be used as page
 * of @c RSBPageViewController must adopt this protocol.
 */
@protocol RSBPage <NSObject>

/*!
 * @discussion Every page must provide placeholder view, instead of it's actual view
 * for smooth scrolling and better UX. Placeholder view must contain minimum of subviews
 * for fastest loading and should not perform any actions on main thread.
 */
@property (nonatomic) UIView *placeholderView;

/*!
 * @discussion This method will be called when page will be reused.
 * @warning This method can be called for page which view was not loaded so you should NOT
 * work with view of view controller inside this method.
 */
- (void)prepareForReuse;

@end
